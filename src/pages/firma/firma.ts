import { Component,ViewChild } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { SignaturePad } from 'angular2-signaturepad';


@Component({
  selector: 'page-firma',
  templateUrl: 'firma.html',
})
export class FirmaPage {

  @ViewChild(SignaturePad) public signaturePad: SignaturePad;

  public signaturePadOptions : Object = {
    'minWidth': 2,
    'canvasWidth': 340,
    'canvasHeight': 200
  };
  public signatureImage : string;
  public converted_image : any = "";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  drawComplete() {
    this.signatureImage = this.signaturePad.toDataURL();
    console.log(this.signatureImage);

    this.converted_image = this.signatureImage;
  }

  drawClear() {
    this.signaturePad.clear();
  }

  canvasResize() {
    let canvas = document.querySelector('canvas');
    this.signaturePad.set('minWidth', 1);
    this.signaturePad.set('canvasWidth', canvas.offsetWidth);
    this.signaturePad.set('canvasHeight', canvas.offsetHeight);
  }

  /*ngAfterViewInit() {
      this.signaturePad.clear();
      this.canvasResize();
  }*/

  ionViewDidLoad() {
    console.log('ionViewDidLoad FirmaPage');
  }

}
